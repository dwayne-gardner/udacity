# Udacity Course and Nanodegree Work

Repository for all udacity work carried out by me. Split into two categories:

## Courses

These are the free supplementary courses that udacity offer as foundations for their Nanodegrees. All work for courses will go inside this folder using the course code as the folder name.

## Nanodegrees

These are where the Nanodegree work and projects will go, once again folders will be divided up by the course code. 
